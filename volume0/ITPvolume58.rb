while n = gets do
    n = n.split(" ").map{|x| x.to_f}
    a = (n[1] - n[3]) / (n[0] - n[2])
    b = (n[5] - n[7]) / (n[4] - n[6])
    if n[1] - n[3] == 0 && n[4] - n[6] == 0
        puts "YES"
    elsif n[0] - n[2] == 0 && n[5] - n[7] == 0
        puts "YES"
    elsif a * b == -1
        puts "YES"
    else
        puts "NO"
    end
end