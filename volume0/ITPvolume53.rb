include Math

def fetch_prime_list(max)
  list = (2..max).to_a
  prime_list = []
  sqrt = Math.sqrt(max).floor

  while val = list.shift
    prime_list << val
    if val > sqrt
      break
    end
    list.delete_if{|num| num % val == 0}
  end
  return prime_list.concat(list)
end

prime = fetch_prime_list(105000)
while true do
    n = gets.to_i
    if n == 0
        break
    end
   
    sum = 0
    for a in 0..n - 1 do
        c = prime[a]
        sum += c.to_i
    end
    puts sum
end

# prime = Array.new
# b = 1
# while true do
#     n = gets.to_i
#     if n == 0
#         break
#     end
#     if b <= n
#         for i in b + 1..10000000 do
#             k = 0
#             prime.each do |j|
#                 if i > j ** (1 / 2)
#                     if i % j == 0
#                         k += 1
#                         break
#                     end
#                 else
#                     k += 1
#                     break
#                 end
#             end
#             if k == 0
#                 prime.push(i)
#             end
#             if prime.count == n
#                 break
#             end
#         end
#         b = n
#     end

#     sum = 0
#     for a in 0..n - 1 do
#         c = prime[a]
#         sum += c.to_i
#     end
#     puts sum
# end