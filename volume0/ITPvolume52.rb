while true do
    n = gets.to_i
    f = 0
    if n == 0
        break
    end
    
    for i in 1..n do
        if i % 5 == 0
            f += 1
        end
        if i % 25 == 0
            f += 1
        end
        if i % 125 == 0
            f += 1
        end
        if i % 625 == 0
            f += 1
        end
        if i % 3125 == 0
            f += 1
        end
        if i % 15625 == 0
            f += 1
        end
    end
    
    puts f
end