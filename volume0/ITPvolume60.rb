while n = gets do
    n = n.split(" ").map{|x| x.to_i}
    a = 20 - n[0] - n[1]
    b = 0
    for i in 1..10 do
        if n[0] == i || n[1] == i || n[2] == i
            if a >= i
                b += 1
            end
        end
    end
    if b >= 3
        puts "YES"
    else
        puts "NO"
    end
end